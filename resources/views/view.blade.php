
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Album example for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="album.css" rel="stylesheet">
  </head>

  <body>

    <div class="container">
      <div class="row">
        <div class="col-md-12 mt-5 bg-dark">

          <form method="POST" action="{{ URL::to("/create-persons")}}">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="exampleInputEmail1">Name</label>
              <input type="text" class="form-control" name="name">
            </div>
            <div class="form-group">
              <label for="exampleInputPassword1">Email</label>
              <input type="text" class="form-control" name="email">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>

                   
        </div>

        
          @foreach( $persons as $item )
          <div class="col-md-3 mt-4 p-0 mr-2">
            <div class="single_info bg-secondary mt-2 mb-2 p-2 text-white text-center">
              <h2>{{ $item->name }}</h2>
              <p>{{ $item->email }}</p>
              <a href="" class="btn btn-success">Edit</a>
              <a href="" class="btn btn-danger">Delete</a>
            </div>
          </div>
          @endforeach
      
          <div class="col-md-12">
            {{ $persons->links() }}
          </div>
        
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
  </body>
</html>
